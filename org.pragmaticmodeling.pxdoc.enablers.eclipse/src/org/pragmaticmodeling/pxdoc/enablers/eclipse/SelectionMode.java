package org.pragmaticmodeling.pxdoc.enablers.eclipse;

public enum SelectionMode {
	file, directory
}
