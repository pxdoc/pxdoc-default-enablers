package org.pragmaticmodeling.pxdoc.enablers.eclipse;

import com.google.inject.Injector;
import fr.pragmaticmodeling.pxdoc.dsl.pxDoc.PxDocGenerator;
import fr.pragmaticmodeling.pxdoc.generator.RuntimeProject;
import java.io.File;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.xtext.generator.model.IXtextGeneratorFileSystemAccess;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUpdateSiteContext;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUpdateSitePrjContext;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxgen.runtime.projects.PlainProject;

@SuppressWarnings("all")
public class EclipseUpdateSitePrj extends PlainProject implements IEclipseUpdateSitePrjContext {
  public EclipseUpdateSitePrj() {
    super();
  }
  
  @Override
  public String getName() {
    return (this.getBasePackage() + ".repository");
  }
  
  public IEclipseUpdateSiteContext getParentContext() {
    return (IEclipseUpdateSiteContext)getParentFragment();
  }
  
  @PxGenParameter
  public void setIncludedFeatures(final List<String> includedFeatures) {
    getParentContext().setIncludedFeatures(includedFeatures);
  }
  
  @PxGenParameter
  public List<String> getIncludedFeatures() {
    return getParentContext().getIncludedFeatures();
  }
  
  @PxGenParameter
  public void setCategoryName(final String categoryName) {
    getParentContext().setCategoryName(categoryName);
  }
  
  @PxGenParameter
  public String getCategoryName() {
    return getParentContext().getCategoryName();
  }
  
  public EclipseUpdateSitePrj getEclipseUpdateSitePrj() {
    return getParentContext().getEclipseUpdateSitePrj();
  }
  
  /**
   * Compute feature name
   */
  public String sourceFeatureName(final String featureId) {
    return getParentContext().sourceFeatureName(featureId);
  }
  
  @PxGenParameter
  public void setBasePackage(final String basePackage) {
    getParentContext().setBasePackage(basePackage);
  }
  
  @PxGenParameter
  public String getBasePackage() {
    return getParentContext().getBasePackage();
  }
  
  @PxGenParameter
  public void setPxDocUri(final String pxDocUri) {
    getParentContext().setPxDocUri(pxDocUri);
  }
  
  @PxGenParameter
  public String getPxDocUri() {
    return getParentContext().getPxDocUri();
  }
  
  @PxGenParameter
  public void setPxDocGenerator(final PxDocGenerator pxDocGenerator) {
    getParentContext().setPxDocGenerator(pxDocGenerator);
  }
  
  @PxGenParameter
  public void setStylesheet(final File stylesheet) {
    getParentContext().setStylesheet(stylesheet);
  }
  
  @PxGenParameter
  public File getStylesheet() {
    return getParentContext().getStylesheet();
  }
  
  @PxGenParameter
  public void setGeneratorClassName(final String generatorClassName) {
    getParentContext().setGeneratorClassName(generatorClassName);
  }
  
  @PxGenParameter
  public String getGeneratorClassName() {
    return getParentContext().getGeneratorClassName();
  }
  
  @PxGenParameter
  public void setLogger(final Logger logger) {
    getParentContext().setLogger(logger);
  }
  
  @PxGenParameter
  public Logger getLogger() {
    return getParentContext().getLogger();
  }
  
  @PxGenParameter
  public void setResourceSet(final XtextResourceSet resourceSet) {
    getParentContext().setResourceSet(resourceSet);
  }
  
  @PxGenParameter
  public XtextResourceSet getResourceSet() {
    return getParentContext().getResourceSet();
  }
  
  @PxGenParameter
  public void setTypeReferences(final TypeReferences typeReferences) {
    getParentContext().setTypeReferences(typeReferences);
  }
  
  @PxGenParameter
  public TypeReferences getTypeReferences() {
    return getParentContext().getTypeReferences();
  }
  
  public Object addDevelopmentTimeBundles(final IGeneratorFragment f) {
    return getParentContext().addDevelopmentTimeBundles(f);
  }
  
  public String getProjectName(final Class<?> clazz) {
    return getParentContext().getProjectName(clazz);
  }
  
  public RuntimeProject getRuntimeProject() {
    return getParentContext().getRuntimeProject();
  }
  
  public String getPxDocGeneratorClassName() {
    return getParentContext().getPxDocGeneratorClassName();
  }
  
  public PxDocGenerator getPxDocGenerator() {
    return getParentContext().getPxDocGenerator();
  }
  
  public PxDocGenerator loadGenerator() {
    return getParentContext().loadGenerator();
  }
  
  public Void setModelObject(final String object) {
    return getParentContext().setModelObject(object);
  }
  
  public IXtextGeneratorFileSystemAccess getRoot() {
    return getFileSystemAccesses().get(".");
  }
  
  public void initialize(final Injector injector) {
    addGenerationDirectory(".", false);
    getFileSystemAccesses().get(".").initialize(injector);
    super.initialize(injector);
    injector.injectMembers(this);
  }
}
