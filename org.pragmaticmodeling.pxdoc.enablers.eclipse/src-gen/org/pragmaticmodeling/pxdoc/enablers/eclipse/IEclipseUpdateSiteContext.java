package org.pragmaticmodeling.pxdoc.enablers.eclipse;

import fr.pragmaticmodeling.pxdoc.generator.IPxDocEnablersContext;
import java.util.List;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.EclipseUpdateSitePrj;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;

@SuppressWarnings("all")
public interface IEclipseUpdateSiteContext extends IPxDocEnablersContext {
  @PxGenParameter
  public abstract List<String> getIncludedFeatures();
  
  @PxGenParameter
  public abstract void setIncludedFeatures(final List<String> includedFeatures);
  
  @PxGenParameter
  public abstract String getCategoryName();
  
  @PxGenParameter
  public abstract void setCategoryName(final String categoryName);
  
  public abstract EclipseUpdateSitePrj getEclipseUpdateSitePrj();
  
  /**
   * Compute feature name
   */
  public abstract String sourceFeatureName(final String featureId);
}
