package org.pragmaticmodeling.pxdoc.enablers.eclipse;

import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;

@SuppressWarnings("all")
public interface IEclipseUiPrjContext extends IEclipseUiContext {
}
