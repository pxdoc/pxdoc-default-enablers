package org.pragmaticmodeling.pxdoc.enablers.eclipse;

import com.google.inject.Injector;
import fr.pragmaticmodeling.pxdoc.dsl.pxDoc.PxDocGenerator;
import fr.pragmaticmodeling.pxdoc.dsl.pxDoc.PxDocGeneratorElement;
import fr.pragmaticmodeling.pxdoc.dsl.pxDoc.Template;
import fr.pragmaticmodeling.pxdoc.generator.IPxDocEnablersContext;
import fr.pragmaticmodeling.pxdoc.generator.RuntimeProject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.EclipseUiPrj;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;
import org.pragmaticmodeling.pxgen.runtime.AbstractGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

@SuppressWarnings("all")
public class EclipseUi extends AbstractGeneratorFragment implements IEclipseUiContext {
  public EclipseUi() {
    super();
  }
  
  public IPxDocEnablersContext getParentContext() {
    return (IPxDocEnablersContext)getParentFragment();
  }
  
  private List<String> pluginEntries;
  
  private List<String> initPluginEntries() {
    ArrayList<String> _arrayList = new ArrayList<String>();
    return _arrayList;
  }
  
  @PxGenParameter
  public List<String> getPluginEntries() {
    if (pluginEntries == null) {
    	pluginEntries = initPluginEntries();	
    }
    return pluginEntries;
  }
  
  @PxGenParameter
  public void setPluginEntries(final List<String> pluginEntries) {
    this.pluginEntries = pluginEntries;
  }
  
  private String executableExtensionFactoryClassName;
  
  private String initExecutableExtensionFactoryClassName() {
    String _basePackage = this.getBasePackage();
    String _plus = (_basePackage + ".ui.");
    String _generatorClassName = this.getGeneratorClassName();
    String _plus_1 = (_plus + _generatorClassName);
    String _plus_2 = (_plus_1 + "ExecutableExtensionFactory");
    return _plus_2;
  }
  
  @PxGenParameter
  public String getExecutableExtensionFactoryClassName() {
    if (executableExtensionFactoryClassName == null) {
    	executableExtensionFactoryClassName = initExecutableExtensionFactoryClassName();	
    }
    return executableExtensionFactoryClassName;
  }
  
  @PxGenParameter
  public void setExecutableExtensionFactoryClassName(final String executableExtensionFactoryClassName) {
    this.executableExtensionFactoryClassName = executableExtensionFactoryClassName;
  }
  
  private String selectionMode;
  
  private String initSelectionMode() {
    return "file";
  }
  
  @PxGenParameter
  public String getSelectionMode() {
    if (selectionMode == null) {
    	selectionMode = initSelectionMode();	
    }
    return selectionMode;
  }
  
  @PxGenParameter
  public void setSelectionMode(final String selectionMode) {
    this.selectionMode = selectionMode;
  }
  
  private String baseCommandClass;
  
  private String initBaseCommandClass() {
    return "org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.AbstractPxDocWizardCommand";
  }
  
  @PxGenParameter
  public String getBaseCommandClass() {
    if (baseCommandClass == null) {
    	baseCommandClass = initBaseCommandClass();	
    }
    return baseCommandClass;
  }
  
  @PxGenParameter
  public void setBaseCommandClass(final String baseCommandClass) {
    this.baseCommandClass = baseCommandClass;
  }
  
  private String commandClassFqn;
  
  private String initCommandClassFqn() {
    String _basePackage = this.getBasePackage();
    String _plus = (_basePackage + ".ui.");
    String _generatorClassName = this.getGeneratorClassName();
    String _plus_1 = (_plus + _generatorClassName);
    String _plus_2 = (_plus_1 + "Command");
    return _plus_2;
  }
  
  @PxGenParameter
  public String getCommandClassFqn() {
    if (commandClassFqn == null) {
    	commandClassFqn = initCommandClassFqn();	
    }
    return commandClassFqn;
  }
  
  @PxGenParameter
  public void setCommandClassFqn(final String commandClassFqn) {
    this.commandClassFqn = commandClassFqn;
  }
  
  private String abstractCommandConstructors;
  
  private String initAbstractCommandConstructors() {
    return "";
  }
  
  @PxGenParameter
  public String getAbstractCommandConstructors() {
    if (abstractCommandConstructors == null) {
    	abstractCommandConstructors = initAbstractCommandConstructors();	
    }
    return abstractCommandConstructors;
  }
  
  @PxGenParameter
  public void setAbstractCommandConstructors(final String abstractCommandConstructors) {
    this.abstractCommandConstructors = abstractCommandConstructors;
  }
  
  private String abstractCommandMethods;
  
  private String initAbstractCommandMethods() {
    return "";
  }
  
  @PxGenParameter
  public String getAbstractCommandMethods() {
    if (abstractCommandMethods == null) {
    	abstractCommandMethods = initAbstractCommandMethods();	
    }
    return abstractCommandMethods;
  }
  
  @PxGenParameter
  public void setAbstractCommandMethods(final String abstractCommandMethods) {
    this.abstractCommandMethods = abstractCommandMethods;
  }
  
  private String commandConstructors;
  
  private String initCommandConstructors() {
    return "";
  }
  
  @PxGenParameter
  public String getCommandConstructors() {
    if (commandConstructors == null) {
    	commandConstructors = initCommandConstructors();	
    }
    return commandConstructors;
  }
  
  @PxGenParameter
  public void setCommandConstructors(final String commandConstructors) {
    this.commandConstructors = commandConstructors;
  }
  
  private String menuLabel;
  
  private String initMenuLabel() {
    String _generatorClassName = this.getGeneratorClassName();
    return _generatorClassName;
  }
  
  @PxGenParameter
  public String getMenuLabel() {
    if (menuLabel == null) {
    	menuLabel = initMenuLabel();	
    }
    return menuLabel;
  }
  
  @PxGenParameter
  public void setMenuLabel(final String menuLabel) {
    this.menuLabel = menuLabel;
  }
  
  private String propertyTesterFqn;
  
  private String initPropertyTesterFqn() {
    return "org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.DefaultPropertyTester";
  }
  
  @PxGenParameter
  public String getPropertyTesterFqn() {
    if (propertyTesterFqn == null) {
    	propertyTesterFqn = initPropertyTesterFqn();	
    }
    return propertyTesterFqn;
  }
  
  @PxGenParameter
  public void setPropertyTesterFqn(final String propertyTesterFqn) {
    this.propertyTesterFqn = propertyTesterFqn;
  }
  
  private String baseModelProvider;
  
  private String initBaseModelProvider() {
    return "fr.pragmaticmodeling.pxdoc.runtime.DefaultModelProvider";
  }
  
  @PxGenParameter
  public String getBaseModelProvider() {
    if (baseModelProvider == null) {
    	baseModelProvider = initBaseModelProvider();	
    }
    return baseModelProvider;
  }
  
  @PxGenParameter
  public void setBaseModelProvider(final String baseModelProvider) {
    this.baseModelProvider = baseModelProvider;
  }
  
  private String abstractCommandClassBody;
  
  @PxGenParameter
  public void setAbstractCommandClassBody(final String abstractCommandClassBody) {
    this.abstractCommandClassBody = abstractCommandClassBody;
  }
  
  public String getCommandObject() {
    {
      PxDocGenerator generator = this.getPxDocGenerator();
      if (((generator != null) && (generator.getBody() != null))) {
        final Function1<PxDocGeneratorElement, Boolean> _function = (PxDocGeneratorElement e) -> {
          return Boolean.valueOf(((e instanceof Template) && ((Template) e).isIsRoot()));
        };
        PxDocGeneratorElement _findFirst = IterableExtensions.<PxDocGeneratorElement>findFirst(generator.getBody().getElements(), _function);
        Template rootTemplate = ((Template) _findFirst);
        if ((rootTemplate != null)) {
          JvmFormalParameter firstParam = IterableExtensions.<JvmFormalParameter>head(rootTemplate.getParams());
          if (((firstParam != null) && (firstParam.getParameterType() != null))) {
            return firstParam.getParameterType().getQualifiedName();
          }
        }
      }
      return "java.lang.Object";
    }
  }
  
  public JvmTypeReference findJvmRef(final String fqn) {
    return null;
  }
  
  public String getAbstractCommandClassBody() {
    String _xifexpression = null;
    if ((this.abstractCommandClassBody == null)) {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package ");
      String _basePackage = this.getBasePackage();
      _builder.append(_basePackage);
      _builder.append(".ui;");
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append("import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.AbstractPxDocWizardCommand;");
      _builder.newLine();
      _builder.newLine();
      _builder.append("public abstract class Abstract");
      String _generatorClassName = this.getGeneratorClassName();
      _builder.append(_generatorClassName);
      _builder.append("Command extends ");
      String _simpleName = this.simpleName(this.baseCommandClass);
      _builder.append(_simpleName);
      _builder.append(" {");
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append(this.abstractCommandConstructors);
      _builder.newLineIfNotEmpty();
      _builder.newLine();
      _builder.append(this.abstractCommandMethods);
      _builder.newLineIfNotEmpty();
      _builder.append("\t");
      _builder.newLine();
      _builder.append("}");
      _xifexpression = this.abstractCommandClassBody = _builder.toString();
    } else {
      return this.abstractCommandClassBody;
    }
    return _xifexpression;
  }
  
  private EclipseUiPrj eclipseUiPrj;
  
  public EclipseUiPrj getEclipseUiPrj() {
    if (eclipseUiPrj == null) {
    	this.eclipseUiPrj = new EclipseUiPrj();
    	this.eclipseUiPrj.setProjectModelName("EclipseUiPrj");
    	this.eclipseUiPrj.setParentFragment(this);
    }
    return eclipseUiPrj;
  }
  
  public String simpleName(final String qualifiedName) {
    {
      int _indexOf = qualifiedName.indexOf(".");
      boolean _greaterThan = (_indexOf > (-1));
      if (_greaterThan) {
        int _lastIndexOf = qualifiedName.lastIndexOf(".");
        int _plus = (_lastIndexOf + 1);
        return qualifiedName.substring(_plus);
      }
      return qualifiedName;
    }
  }
  
  public String javaPackage(final String qualifiedName) {
    {
      int _indexOf = qualifiedName.indexOf(".");
      boolean _greaterThan = (_indexOf > (-1));
      if (_greaterThan) {
        return qualifiedName.substring(0, qualifiedName.indexOf("."));
      }
      return qualifiedName;
    }
  }
  
  public String generateExtensions() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("<!-- Commands -->");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<extension point=\"org.eclipse.ui.commands\">  ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<command");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("id=\"");
    _builder.append(this.commandClassFqn, "\t\t\t");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("name=\"");
    String _simpleName = this.simpleName(this.commandClassFqn);
    _builder.append(_simpleName, "\t\t\t");
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("</command>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</extension>");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<!-- Handlers -->");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<extension point=\"org.eclipse.ui.handlers\">");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<handler");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("class=\"");
    _builder.append(this.executableExtensionFactoryClassName, "\t\t\t");
    _builder.append(":");
    _builder.append(this.commandClassFqn, "\t\t\t");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("commandId=\"");
    _builder.append(this.commandClassFqn, "\t\t\t");
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("</handler>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</extension>");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<!-- Menus -->");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<extension point=\"org.eclipse.ui.menus\">");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<menuContribution");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("allPopups=\"true\"");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("locationURI=\"popup:fr.pragmaticmodeling.common.menu\">");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("<command");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("commandId=\"");
    _builder.append(this.commandClassFqn, "\t\t\t\t");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t");
    _builder.append("label=\"%menuLabel\" ");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("style=\"push\">");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("<visibleWhen   ");
    _builder.newLine();
    _builder.append("\t\t\t\t\t");
    _builder.append("checkEnabled=\"false\">");
    _builder.newLine();
    _builder.append("\t\t\t\t\t");
    _builder.append("<iterate>");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t");
    _builder.append("<test property=\"");
    String _basePackage = this.getBasePackage();
    _builder.append(_basePackage, "\t\t\t\t\t\t");
    _builder.append(".ui.canLaunchPxDoc\" forcePluginActivation=\"true\"/>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t\t");
    _builder.append("</iterate>");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("</visibleWhen>");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("</command>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("</menuContribution>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</extension>");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<extension");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("point=\"org.eclipse.core.expressions.propertyTesters\">");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<propertyTester");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("class=\"");
    String _basePackage_1 = this.getBasePackage();
    _builder.append(_basePackage_1, "\t\t\t");
    _builder.append(".ui.");
    String _generatorClassName = this.getGeneratorClassName();
    _builder.append(_generatorClassName, "\t\t\t");
    _builder.append("PropertyTester\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("id=\"");
    String _basePackage_2 = this.getBasePackage();
    _builder.append(_basePackage_2, "\t\t\t");
    _builder.append(".ui.");
    String _generatorClassName_1 = this.getGeneratorClassName();
    _builder.append(_generatorClassName_1, "\t\t\t");
    _builder.append("PropertyTester\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("namespace=\"");
    String _basePackage_3 = this.getBasePackage();
    _builder.append(_basePackage_3, "\t\t\t");
    _builder.append(".ui\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("properties=\"canLaunchPxDoc\"");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("type=\"java.lang.Object\">");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("</propertyTester>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</extension>");
    return _builder.toString();
  }
  
  @PxGenParameter
  public void setBasePackage(final String basePackage) {
    getParentContext().setBasePackage(basePackage);
  }
  
  @PxGenParameter
  public String getBasePackage() {
    return getParentContext().getBasePackage();
  }
  
  @PxGenParameter
  public void setPxDocUri(final String pxDocUri) {
    getParentContext().setPxDocUri(pxDocUri);
  }
  
  @PxGenParameter
  public String getPxDocUri() {
    return getParentContext().getPxDocUri();
  }
  
  @PxGenParameter
  public void setPxDocGenerator(final PxDocGenerator pxDocGenerator) {
    getParentContext().setPxDocGenerator(pxDocGenerator);
  }
  
  @PxGenParameter
  public void setStylesheet(final File stylesheet) {
    getParentContext().setStylesheet(stylesheet);
  }
  
  @PxGenParameter
  public File getStylesheet() {
    return getParentContext().getStylesheet();
  }
  
  @PxGenParameter
  public void setGeneratorClassName(final String generatorClassName) {
    getParentContext().setGeneratorClassName(generatorClassName);
  }
  
  @PxGenParameter
  public String getGeneratorClassName() {
    return getParentContext().getGeneratorClassName();
  }
  
  @PxGenParameter
  public void setLogger(final Logger logger) {
    getParentContext().setLogger(logger);
  }
  
  @PxGenParameter
  public Logger getLogger() {
    return getParentContext().getLogger();
  }
  
  @PxGenParameter
  public void setResourceSet(final XtextResourceSet resourceSet) {
    getParentContext().setResourceSet(resourceSet);
  }
  
  @PxGenParameter
  public XtextResourceSet getResourceSet() {
    return getParentContext().getResourceSet();
  }
  
  @PxGenParameter
  public void setTypeReferences(final TypeReferences typeReferences) {
    getParentContext().setTypeReferences(typeReferences);
  }
  
  @PxGenParameter
  public TypeReferences getTypeReferences() {
    return getParentContext().getTypeReferences();
  }
  
  public Object addDevelopmentTimeBundles(final IGeneratorFragment f) {
    return getParentContext().addDevelopmentTimeBundles(f);
  }
  
  public String getProjectName(final Class<?> clazz) {
    return getParentContext().getProjectName(clazz);
  }
  
  public RuntimeProject getRuntimeProject() {
    return getParentContext().getRuntimeProject();
  }
  
  public String getPxDocGeneratorClassName() {
    return getParentContext().getPxDocGeneratorClassName();
  }
  
  public PxDocGenerator getPxDocGenerator() {
    return getParentContext().getPxDocGenerator();
  }
  
  public PxDocGenerator loadGenerator() {
    return getParentContext().loadGenerator();
  }
  
  public Void setModelObject(final String object) {
    return getParentContext().setModelObject(object);
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
    addProject(getEclipseUiPrj());
    for (ProjectDescriptor project : getProjects()) {
    	project.initialize(injector);
    }
    for (IGeneratorFragment f : getFragments()) {
    	f.initialize(injector);
    	//injector.injectMembers(f);
    }
    if (this.pluginEntries == null) {
    	this.pluginEntries = initPluginEntries();
    }
    if (this.executableExtensionFactoryClassName == null) {
    	this.executableExtensionFactoryClassName = initExecutableExtensionFactoryClassName();
    }
    if (this.selectionMode == null) {
    	this.selectionMode = initSelectionMode();
    }
    if (this.baseCommandClass == null) {
    	this.baseCommandClass = initBaseCommandClass();
    }
    if (this.commandClassFqn == null) {
    	this.commandClassFqn = initCommandClassFqn();
    }
    if (this.abstractCommandConstructors == null) {
    	this.abstractCommandConstructors = initAbstractCommandConstructors();
    }
    if (this.abstractCommandMethods == null) {
    	this.abstractCommandMethods = initAbstractCommandMethods();
    }
    if (this.commandConstructors == null) {
    	this.commandConstructors = initCommandConstructors();
    }
    if (this.menuLabel == null) {
    	this.menuLabel = initMenuLabel();
    }
    if (this.propertyTesterFqn == null) {
    	this.propertyTesterFqn = initPropertyTesterFqn();
    }
    if (this.baseModelProvider == null) {
    	this.baseModelProvider = initBaseModelProvider();
    }
  }
  
  @Override
  public void doGenerate() {
    {
      this.eclipseUiPrj.setNameQualifier(".ui");
      List<String> _requiredBundles = this.eclipseUiPrj.getRequiredBundles();
      _requiredBundles.add("org.pragmaticmodeling.pxdoc.runtime.ui.eclipse");
      List<String> _requiredBundles_1 = this.eclipseUiPrj.getRequiredBundles();
      String _name = this.getRuntimeProject().getName();
      _requiredBundles_1.add(_name);
      List<String> _importedPackages = this.eclipseUiPrj.getImportedPackages();
      _importedPackages.add("com.google.inject");
      List<String> _importedPackages_1 = this.eclipseUiPrj.getImportedPackages();
      _importedPackages_1.add("com.google.inject.binder");
      List<String> _importedPackages_2 = this.eclipseUiPrj.getImportedPackages();
      _importedPackages_2.add("org.apache.log4j");
      String _basePackage = this.getBasePackage();
      String _plus = (_basePackage + ".ui.");
      String _generatorClassName = this.getGeneratorClassName();
      String _plus_1 = (_plus + _generatorClassName);
      String _plus_2 = (_plus_1 + "Activator");
      this.eclipseUiPrj.setActivatorClassName(_plus_2);
      List<String> _exportedPackages = this.eclipseUiPrj.getExportedPackages();
      String _basePackage_1 = this.getBasePackage();
      String _plus_3 = (_basePackage_1 + ".ui");
      _exportedPackages.add(_plus_3);
      this.eclipseUiPrj.getBundleProperties().put("menuLabel", this.menuLabel);
      String basePath = this.getBasePackage().replaceAll("\\.", "/");
      String _generateExtensions = this.generateExtensions();
      this.pluginEntries.add(_generateExtensions);
      // generate file PropertyTester
      getEclipseUiPrj().getSrc().generateFile((((basePath + "/ui/") + this.getGeneratorClassName()) + "PropertyTester.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.PropertyTesterFile(this).getContent());
      // generate file AbstractPropertyTester
      getEclipseUiPrj().getSrcGen().generateFile((((basePath + "/ui/Abstract") + this.getGeneratorClassName()) + "PropertyTester.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.AbstractPropertyTesterFile(this).getContent());
      // generate file Activator
      getEclipseUiPrj().getSrc().generateFile((((basePath + "/ui/") + this.getGeneratorClassName()) + "Activator.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.ActivatorFile(getEclipseUiPrj()).getContent());
      // generate file EclipseUiModule
      getEclipseUiPrj().getSrc().generateFile((((basePath + "/ui/") + this.getGeneratorClassName()) + "UiModule.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.EclipseUiModuleFile(this).getContent());
      // generate file AbstractUiModule
      getEclipseUiPrj().getSrcGen().generateFile((((basePath + "/ui/Abstract") + this.getGeneratorClassName()) + "UiModule.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.AbstractUiModuleFile(this).getContent());
      // generate file ExecutableExtensionFactory
      getEclipseUiPrj().getSrcGen().generateFile((((basePath + "/ui/") + this.getGeneratorClassName()) + "ExecutableExtensionFactory.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.ExecutableExtensionFactoryFile(this).getContent());
      // generate file AbstractWizard
      getEclipseUiPrj().getSrcGen().generateFile((((basePath + "/ui/Abstract") + this.getGeneratorClassName()) + "Wizard.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.AbstractWizardFile(this).getContent());
      // generate file Wizard
      getEclipseUiPrj().getSrc().generateFile((((basePath + "/ui/") + this.getGeneratorClassName()) + "Wizard.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.WizardFile(this).getContent());
      // generate file PluginXml
      getEclipseUiPrj().getRoot().generateFile("plugin.xml", new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.PluginXmlFile(this).getContent());
      // generate file AbstractCommandClass
      getEclipseUiPrj().getSrcGen().generateFile((((basePath + "/ui/Abstract") + this.getGeneratorClassName()) + "Command.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.AbstractCommandClassFile(getEclipseUiPrj()).getContent());
      // generate file CommandClass
      getEclipseUiPrj().getSrc().generateFile((((basePath + "/ui/") + this.getGeneratorClassName()) + "Command.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.CommandClassFile(getEclipseUiPrj()).getContent());
      // generate file ModelProvider
      getEclipseUiPrj().getSrc().generateFile((((basePath + "/ui/") + this.getGeneratorClassName()) + "ModelProvider.java"), new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.ModelProviderFile(this, this.baseModelProvider).getContent());
    }
  }
}
