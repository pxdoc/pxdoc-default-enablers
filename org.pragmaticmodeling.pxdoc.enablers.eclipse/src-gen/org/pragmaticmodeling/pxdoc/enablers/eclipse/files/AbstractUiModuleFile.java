package org.pragmaticmodeling.pxdoc.enablers.eclipse.files;

import com.google.inject.Injector;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;
import org.pragmaticmodeling.pxgen.runtime.IContext;

@SuppressWarnings("all")
public class AbstractUiModuleFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IEclipseUiContext context;
  
  public AbstractUiModuleFile(final IEclipseUiContext context) {
    super();
    this.context = context;
  }
  
  public IEclipseUiContext getContext() {
    return this.context;
  }
  
  public void setContext(final IEclipseUiContext context) {
    this.context = context;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _basePackage = this.context.getBasePackage();
    _builder.append(_basePackage);
    _builder.append(".ui;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("import org.eclipse.jface.dialogs.IDialogSettings;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import com.google.inject.Binder;");
    _builder.newLine();
    _builder.append("import com.google.inject.Module;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import ");
    String _basePackage_1 = this.context.getBasePackage();
    _builder.append(_basePackage_1);
    _builder.append(".");
    String _generatorClassName = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;");
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.IPxDocGenerator;");
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.IPxDocLanguageRenderersManager;");
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.IResourceProvider;");
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.IStylesheetsRegistry;");
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.eclipse.EclipseResourceProvider;");
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.eclipse.StylesheetRegistryImpl;");
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.eclipse.internal.language.renderers.EclipseLanguageRenderersManager;");
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.eclipse.preferences.PxDocEclipsePreferences;");
    _builder.newLine();
    _builder.append("import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;");
    _builder.newLine();
    _builder.append("import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.wizard.IPxDocWizard;");
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.preferences.IPxDocPreferences;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public abstract class Abstract");
    String _generatorClassName_1 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_1);
    _builder.append("UiModule implements Module {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("private final AbstractPxUiPlugin plugin;");
    _builder.newLine();
    _builder.append("\t\t   ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public Abstract");
    String _generatorClassName_2 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_2, "\t");
    _builder.append("UiModule(AbstractPxUiPlugin plugin) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("this.plugin = plugin;");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public void configure(Binder binder) {");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("binder.bind(AbstractPxUiPlugin.class).toInstance(plugin);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("binder.bind(IDialogSettings.class).toInstance(plugin.getDialogSettings());");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("binder.bind(IStylesheetsRegistry.class).to(StylesheetRegistryImpl.class);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("binder.bind(IPxDocLanguageRenderersManager.class).to(EclipseLanguageRenderersManager.class);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("binder.bind(IPxDocGenerator.class).to(");
    String _generatorClassName_3 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_3, "\t\t");
    _builder.append(".class);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("binder.bind(IResourceProvider.class).to(EclipseResourceProvider.class);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("binder.bind(IPxDocPreferences.class).to(PxDocEclipsePreferences.class);");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("binder.bind(IModelProvider.class).to(");
    String _generatorClassName_4 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_4, "\t\t");
    _builder.append("ModelProvider.class);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("binder.bind(IPxDocWizard.class).to(");
    String _generatorClassName_5 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_5, "\t\t");
    _builder.append("Wizard.class);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("}\t");
    _builder.newLine();
    return _builder.toString();
  }
}
