package org.pragmaticmodeling.pxdoc.enablers.eclipse.files;

import com.google.inject.Injector;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;
import org.pragmaticmodeling.pxgen.runtime.IContext;

@SuppressWarnings("all")
public class ExecutableExtensionFactoryFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IEclipseUiContext context;
  
  public ExecutableExtensionFactoryFile(final IEclipseUiContext context) {
    super();
    this.context = context;
  }
  
  public IEclipseUiContext getContext() {
    return this.context;
  }
  
  public void setContext(final IEclipseUiContext context) {
    this.context = context;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _basePackage = this.context.getBasePackage();
    _builder.append(_basePackage);
    _builder.append(".ui;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("import org.osgi.framework.Bundle;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import com.google.inject.Injector;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("import fr.pragmaticmodeling.pxdoc.runtime.eclipse.guice.AbstractGuiceAwareExecutableExtensionFactory;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public class ");
    String _generatorClassName = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName);
    _builder.append("ExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t   ");
    _builder.append("protected Bundle getBundle() {");
    _builder.newLine();
    _builder.append("\t       ");
    _builder.append("return ");
    String _generatorClassName_1 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_1, "\t       ");
    _builder.append("Activator.getInstance().getBundle();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t   ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t    ");
    _builder.newLine();
    _builder.append("\t   ");
    _builder.append("@Override");
    _builder.newLine();
    _builder.append("\t   ");
    _builder.append("protected Injector getInjector() {");
    _builder.newLine();
    _builder.append("\t       ");
    _builder.append("return ");
    String _generatorClassName_2 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_2, "\t       ");
    _builder.append("Activator.getInstance().getInjector();");
    _builder.newLineIfNotEmpty();
    _builder.append("\t   ");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t   ");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder.toString();
  }
}
