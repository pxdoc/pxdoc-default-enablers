package org.pragmaticmodeling.pxdoc.enablers.eclipse.files;

import com.google.inject.Injector;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiPrjContext;
import org.pragmaticmodeling.pxgen.runtime.IContext;

@SuppressWarnings("all")
public class AbstractCommandClassFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IEclipseUiPrjContext context;
  
  public AbstractCommandClassFile(final IEclipseUiPrjContext context) {
    super();
    this.context = context;
  }
  
  public IEclipseUiPrjContext getContext() {
    return this.context;
  }
  
  public void setContext(final IEclipseUiPrjContext context) {
    this.context = context;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    String _abstractCommandClassBody = this.context.getAbstractCommandClassBody();
    _builder.append(_abstractCommandClassBody);
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    return _builder.toString();
  }
}
