package org.pragmaticmodeling.pxdoc.enablers.eclipse.files;

import com.google.inject.Injector;
import java.util.List;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUpdateSiteContext;
import org.pragmaticmodeling.pxgen.runtime.IContext;

@SuppressWarnings("all")
public class CategoryFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IEclipseUpdateSiteContext context;
  
  public CategoryFile(final IEclipseUpdateSiteContext context) {
    super();
    this.context = context;
  }
  
  public IEclipseUpdateSiteContext getContext() {
    return this.context;
  }
  
  public void setContext(final IEclipseUpdateSiteContext context) {
    this.context = context;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
    _builder.newLine();
    _builder.append("<site>");
    _builder.newLine();
    {
      List<String> _includedFeatures = this.context.getIncludedFeatures();
      for(final String includedFeature : _includedFeatures) {
        _builder.append("\t");
        _builder.append("<feature id=\"");
        _builder.append(includedFeature, "\t");
        _builder.append("\" version=\"0.0.0\">");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<category name=\"main\"/>");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("</feature>");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("<feature id=\"");
        String _sourceFeatureName = this.context.sourceFeatureName(includedFeature);
        _builder.append(_sourceFeatureName, "\t");
        _builder.append("\" version=\"0.0.0\">");
        _builder.newLineIfNotEmpty();
        _builder.append("\t");
        _builder.append("\t");
        _builder.append("<category name=\"main.source\"/>");
        _builder.newLine();
        _builder.append("\t");
        _builder.append("</feature>");
        _builder.newLine();
      }
    }
    _builder.append("\t  ");
    _builder.append("<category-def name=\"main\" label=\"");
    String _categoryName = this.context.getCategoryName();
    _builder.append(_categoryName, "\t  ");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    _builder.append("\t  ");
    _builder.append("<category-def name=\"main.source\" label=\"Source for ");
    String _categoryName_1 = this.context.getCategoryName();
    _builder.append(_categoryName_1, "\t  ");
    _builder.append("\"/>");
    _builder.newLineIfNotEmpty();
    _builder.append("</site>");
    _builder.newLine();
    return _builder.toString();
  }
}
