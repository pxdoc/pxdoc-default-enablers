package org.pragmaticmodeling.pxdoc.enablers.eclipse.files;

import com.google.inject.Injector;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;
import org.pragmaticmodeling.pxgen.runtime.IContext;

@SuppressWarnings("all")
public class EclipseUiModuleFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IEclipseUiContext context;
  
  public EclipseUiModuleFile(final IEclipseUiContext context) {
    super();
    this.context = context;
  }
  
  public IEclipseUiContext getContext() {
    return this.context;
  }
  
  public void setContext(final IEclipseUiContext context) {
    this.context = context;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _basePackage = this.context.getBasePackage();
    _builder.append(_basePackage);
    _builder.append(".ui;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.AbstractPxUiPlugin;");
    _builder.newLine();
    _builder.newLine();
    _builder.append("public class ");
    String _generatorClassName = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName);
    _builder.append("UiModule extends Abstract");
    String _generatorClassName_1 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_1);
    _builder.append("UiModule {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t   ");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public ");
    String _generatorClassName_2 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_2, "\t");
    _builder.append("UiModule(AbstractPxUiPlugin plugin) {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("super(plugin);");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    return _builder.toString();
  }
}
