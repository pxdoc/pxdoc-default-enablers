package org.pragmaticmodeling.pxdoc.enablers.eclipse.files;

import com.google.inject.Injector;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiPrjContext;
import org.pragmaticmodeling.pxgen.runtime.IContext;

@SuppressWarnings("all")
public class CommandClassFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IEclipseUiPrjContext context;
  
  public CommandClassFile(final IEclipseUiPrjContext context) {
    super();
    this.context = context;
  }
  
  public IEclipseUiPrjContext getContext() {
    return this.context;
  }
  
  public void setContext(final IEclipseUiPrjContext context) {
    this.context = context;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _basePackage = this.context.getBasePackage();
    _builder.append(_basePackage);
    _builder.append(".ui;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("public class ");
    String _generatorClassName = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName);
    _builder.append("Command extends Abstract");
    String _generatorClassName_1 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_1);
    _builder.append("Command {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.newLine();
    String _commandConstructors = this.context.getCommandConstructors();
    _builder.append(_commandConstructors);
    _builder.newLineIfNotEmpty();
    _builder.append("}");
    _builder.newLine();
    return _builder.toString();
  }
}
