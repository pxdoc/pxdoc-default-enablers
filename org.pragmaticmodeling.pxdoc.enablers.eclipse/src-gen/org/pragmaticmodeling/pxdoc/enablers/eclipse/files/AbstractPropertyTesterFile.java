package org.pragmaticmodeling.pxdoc.enablers.eclipse.files;

import com.google.inject.Injector;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xtext.generator.IGuiceAwareGeneratorComponent;
import org.eclipse.xtext.xtext.generator.model.TextFileAccess;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;
import org.pragmaticmodeling.pxgen.runtime.IContext;

@SuppressWarnings("all")
public class AbstractPropertyTesterFile extends TextFileAccess implements IGuiceAwareGeneratorComponent, IContext {
  private IEclipseUiContext context;
  
  public AbstractPropertyTesterFile(final IEclipseUiContext context) {
    super();
    this.context = context;
  }
  
  public IEclipseUiContext getContext() {
    return this.context;
  }
  
  public void setContext(final IEclipseUiContext context) {
    this.context = context;
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
  }
  
  public String getContent() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("package ");
    String _basePackage = this.context.getBasePackage();
    _builder.append(_basePackage);
    _builder.append(".ui;");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("import ");
    String _propertyTesterFqn = this.context.getPropertyTesterFqn();
    _builder.append(_propertyTesterFqn);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.append("import ");
    String _commandObject = this.context.getCommandObject();
    _builder.append(_commandObject);
    _builder.append(";");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("/**");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("* PxDocModel property tester and model provider");
    _builder.newLine();
    _builder.append(" ");
    _builder.append("*/");
    _builder.newLine();
    _builder.append("public class Abstract");
    String _generatorClassName = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName);
    _builder.append("PropertyTester extends ");
    String _simpleName = this.context.simpleName(this.context.getPropertyTesterFqn());
    _builder.append(_simpleName);
    _builder.append(" {");
    _builder.newLineIfNotEmpty();
    _builder.newLine();
    _builder.append("\t");
    _builder.append("public Abstract");
    String _generatorClassName_1 = this.context.getGeneratorClassName();
    _builder.append(_generatorClassName_1, "\t");
    _builder.append("PropertyTester() {");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("super(");
    String _simpleName_1 = this.context.simpleName(this.context.getCommandObject());
    _builder.append(_simpleName_1, "\t\t");
    _builder.append(".class);");
    _builder.newLineIfNotEmpty();
    _builder.append("\t");
    _builder.append("}");
    _builder.newLine();
    _builder.append("}");
    _builder.newLine();
    _builder.newLine();
    return _builder.toString();
  }
}
