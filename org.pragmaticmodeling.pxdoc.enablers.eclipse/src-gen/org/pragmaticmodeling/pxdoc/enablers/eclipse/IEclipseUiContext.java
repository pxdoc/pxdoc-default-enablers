package org.pragmaticmodeling.pxdoc.enablers.eclipse;

import fr.pragmaticmodeling.pxdoc.generator.IPxDocEnablersContext;
import java.util.List;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.EclipseUiPrj;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;

@SuppressWarnings("all")
public interface IEclipseUiContext extends IPxDocEnablersContext {
  @PxGenParameter
  public abstract List<String> getPluginEntries();
  
  @PxGenParameter
  public abstract void setPluginEntries(final List<String> pluginEntries);
  
  @PxGenParameter
  public abstract String getExecutableExtensionFactoryClassName();
  
  @PxGenParameter
  public abstract void setExecutableExtensionFactoryClassName(final String executableExtensionFactoryClassName);
  
  @PxGenParameter
  public abstract String getSelectionMode();
  
  @PxGenParameter
  public abstract void setSelectionMode(final String selectionMode);
  
  @PxGenParameter
  public abstract String getBaseCommandClass();
  
  @PxGenParameter
  public abstract void setBaseCommandClass(final String baseCommandClass);
  
  @PxGenParameter
  public abstract String getCommandClassFqn();
  
  @PxGenParameter
  public abstract void setCommandClassFqn(final String commandClassFqn);
  
  @PxGenParameter
  public abstract String getAbstractCommandConstructors();
  
  @PxGenParameter
  public abstract void setAbstractCommandConstructors(final String abstractCommandConstructors);
  
  @PxGenParameter
  public abstract String getAbstractCommandMethods();
  
  @PxGenParameter
  public abstract void setAbstractCommandMethods(final String abstractCommandMethods);
  
  @PxGenParameter
  public abstract String getCommandConstructors();
  
  @PxGenParameter
  public abstract void setCommandConstructors(final String commandConstructors);
  
  @PxGenParameter
  public abstract String getMenuLabel();
  
  @PxGenParameter
  public abstract void setMenuLabel(final String menuLabel);
  
  @PxGenParameter
  public abstract String getPropertyTesterFqn();
  
  @PxGenParameter
  public abstract void setPropertyTesterFqn(final String propertyTesterFqn);
  
  @PxGenParameter
  public abstract String getBaseModelProvider();
  
  @PxGenParameter
  public abstract void setBaseModelProvider(final String baseModelProvider);
  
  @PxGenParameter
  public abstract void setAbstractCommandClassBody(final String abstractCommandClassBody);
  
  public abstract String getCommandObject();
  
  public abstract JvmTypeReference findJvmRef(final String fqn);
  
  public abstract String getAbstractCommandClassBody();
  
  public abstract EclipseUiPrj getEclipseUiPrj();
  
  public abstract String simpleName(final String qualifiedName);
  
  public abstract String javaPackage(final String qualifiedName);
  
  public abstract String generateExtensions();
}
