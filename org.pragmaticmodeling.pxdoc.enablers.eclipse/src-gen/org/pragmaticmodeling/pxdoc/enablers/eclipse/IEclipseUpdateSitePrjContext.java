package org.pragmaticmodeling.pxdoc.enablers.eclipse;

import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUpdateSiteContext;

@SuppressWarnings("all")
public interface IEclipseUpdateSitePrjContext extends IEclipseUpdateSiteContext {
}
