package org.pragmaticmodeling.pxdoc.enablers.eclipse;

import com.google.common.collect.Iterables;
import com.google.inject.Injector;
import fr.pragmaticmodeling.pxdoc.dsl.pxDoc.PxDocGenerator;
import fr.pragmaticmodeling.pxdoc.generator.IPxDocEnablersContext;
import fr.pragmaticmodeling.pxdoc.generator.RuntimeProject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.EclipseUpdateSitePrj;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUpdateSiteContext;
import org.pragmaticmodeling.pxgen.runtime.AbstractGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxgen.runtime.projects.FeatureProject;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

@SuppressWarnings("all")
public class EclipseUpdateSite extends AbstractGeneratorFragment implements IEclipseUpdateSiteContext {
  public EclipseUpdateSite() {
    super();
  }
  
  public IPxDocEnablersContext getParentContext() {
    return (IPxDocEnablersContext)getParentFragment();
  }
  
  private List<String> includedFeatures;
  
  private List<String> initIncludedFeatures() {
    ArrayList<String> _newArrayList = CollectionLiterals.<String>newArrayList();
    return _newArrayList;
  }
  
  @PxGenParameter
  public List<String> getIncludedFeatures() {
    if (includedFeatures == null) {
    	includedFeatures = initIncludedFeatures();	
    }
    return includedFeatures;
  }
  
  @PxGenParameter
  public void setIncludedFeatures(final List<String> includedFeatures) {
    this.includedFeatures = includedFeatures;
  }
  
  private String categoryName;
  
  private String initCategoryName() {
    String _generatorClassName = this.getGeneratorClassName();
    return _generatorClassName;
  }
  
  @PxGenParameter
  public String getCategoryName() {
    if (categoryName == null) {
    	categoryName = initCategoryName();	
    }
    return categoryName;
  }
  
  @PxGenParameter
  public void setCategoryName(final String categoryName) {
    this.categoryName = categoryName;
  }
  
  private EclipseUpdateSitePrj eclipseUpdateSitePrj;
  
  public EclipseUpdateSitePrj getEclipseUpdateSitePrj() {
    if (eclipseUpdateSitePrj == null) {
    	this.eclipseUpdateSitePrj = new EclipseUpdateSitePrj();
    	this.eclipseUpdateSitePrj.setProjectModelName("EclipseUpdateSitePrj");
    	this.eclipseUpdateSitePrj.setParentFragment(this);
    }
    return eclipseUpdateSitePrj;
  }
  
  /**
   * Compute feature name
   */
  public String sourceFeatureName(final String featureId) {
    String _xblockexpression = null;
    {
      String featureEnd = ".feature";
      String sourceFeatureEnd = ".source";
      boolean _endsWith = featureId.endsWith(featureEnd);
      if (_endsWith) {
        return featureId.replaceAll((("\\" + featureEnd) + "$"), (sourceFeatureEnd + featureEnd));
      }
      _xblockexpression = (featureId + sourceFeatureEnd);
    }
    return _xblockexpression;
  }
  
  @PxGenParameter
  public void setBasePackage(final String basePackage) {
    getParentContext().setBasePackage(basePackage);
  }
  
  @PxGenParameter
  public String getBasePackage() {
    return getParentContext().getBasePackage();
  }
  
  @PxGenParameter
  public void setPxDocUri(final String pxDocUri) {
    getParentContext().setPxDocUri(pxDocUri);
  }
  
  @PxGenParameter
  public String getPxDocUri() {
    return getParentContext().getPxDocUri();
  }
  
  @PxGenParameter
  public void setPxDocGenerator(final PxDocGenerator pxDocGenerator) {
    getParentContext().setPxDocGenerator(pxDocGenerator);
  }
  
  @PxGenParameter
  public void setStylesheet(final File stylesheet) {
    getParentContext().setStylesheet(stylesheet);
  }
  
  @PxGenParameter
  public File getStylesheet() {
    return getParentContext().getStylesheet();
  }
  
  @PxGenParameter
  public void setGeneratorClassName(final String generatorClassName) {
    getParentContext().setGeneratorClassName(generatorClassName);
  }
  
  @PxGenParameter
  public String getGeneratorClassName() {
    return getParentContext().getGeneratorClassName();
  }
  
  @PxGenParameter
  public void setLogger(final Logger logger) {
    getParentContext().setLogger(logger);
  }
  
  @PxGenParameter
  public Logger getLogger() {
    return getParentContext().getLogger();
  }
  
  @PxGenParameter
  public void setResourceSet(final XtextResourceSet resourceSet) {
    getParentContext().setResourceSet(resourceSet);
  }
  
  @PxGenParameter
  public XtextResourceSet getResourceSet() {
    return getParentContext().getResourceSet();
  }
  
  @PxGenParameter
  public void setTypeReferences(final TypeReferences typeReferences) {
    getParentContext().setTypeReferences(typeReferences);
  }
  
  @PxGenParameter
  public TypeReferences getTypeReferences() {
    return getParentContext().getTypeReferences();
  }
  
  public Object addDevelopmentTimeBundles(final IGeneratorFragment f) {
    return getParentContext().addDevelopmentTimeBundles(f);
  }
  
  public String getProjectName(final Class<?> clazz) {
    return getParentContext().getProjectName(clazz);
  }
  
  public RuntimeProject getRuntimeProject() {
    return getParentContext().getRuntimeProject();
  }
  
  public String getPxDocGeneratorClassName() {
    return getParentContext().getPxDocGeneratorClassName();
  }
  
  public PxDocGenerator getPxDocGenerator() {
    return getParentContext().getPxDocGenerator();
  }
  
  public PxDocGenerator loadGenerator() {
    return getParentContext().loadGenerator();
  }
  
  public Void setModelObject(final String object) {
    return getParentContext().setModelObject(object);
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
    addProject(getEclipseUpdateSitePrj());
    for (ProjectDescriptor project : getProjects()) {
    	project.initialize(injector);
    }
    for (IGeneratorFragment f : getFragments()) {
    	f.initialize(injector);
    	//injector.injectMembers(f);
    }
    if (this.includedFeatures == null) {
    	this.includedFeatures = initIncludedFeatures();
    }
    if (this.categoryName == null) {
    	this.categoryName = initCategoryName();
    }
  }
  
  @Override
  public void doGenerate() {
    {
      final Function1<ProjectDescriptor, Boolean> _function = (ProjectDescriptor p) -> {
        return Boolean.valueOf((p instanceof FeatureProject));
      };
      final Function1<ProjectDescriptor, String> _function_1 = (ProjectDescriptor it) -> {
        return it.getName();
      };
      Iterable<String> _map = IterableExtensions.<ProjectDescriptor, String>map(IterableExtensions.<ProjectDescriptor>filter(this.getGenerator().getAllProjects(), _function), _function_1);
      Iterables.<String>addAll(this.includedFeatures, _map);
      // generate file Category
      getEclipseUpdateSitePrj().getRoot().generateFile("site.xml", new org.pragmaticmodeling.pxdoc.enablers.eclipse.files.CategoryFile(this).getContent());
    }
  }
}
