package org.pragmaticmodeling.pxdoc.enablers.eclipse;

import fr.pragmaticmodeling.pxdoc.generator.IPxDocEnablersContext;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.EclipseFeaturePrj;

@SuppressWarnings("all")
public interface IEclipseFeatureContext extends IPxDocEnablersContext {
  public abstract EclipseFeaturePrj getEclipseFeaturePrj();
}
