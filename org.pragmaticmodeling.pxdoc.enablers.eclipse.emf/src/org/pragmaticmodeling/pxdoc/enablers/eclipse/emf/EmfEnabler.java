package org.pragmaticmodeling.pxdoc.enablers.eclipse.emf;

import java.util.ArrayList;
import java.util.List;

import fr.pragmaticmodeling.pxdoc.dsl.ui.enablers.AbstractPlatformEnabler;

public class EmfEnabler extends AbstractPlatformEnabler {

	public EmfEnabler() {
		super();
	}

	@Override
	public List<String> getWithModules() {
		List<String> modules = new ArrayList<String>();
		modules.add("org.pragmaticmodeling.pxdoc.common.lib.CommonServices");
		modules.add("org.pragmaticmodeling.pxdoc.emf.lib.EmfServices");
		return modules;
	}

	@Override
	public String getModelObject() {
		return "org.eclipse.emf.ecore.EObject";
	}

	@Override
	public String getJavaClass() {
		return "org.pragmaticmodeling.pxdoc.enablers.eclipse.emf.EmfInput";
	}

	@Override
	public List<String> getDevelopmentTimesBundles() {
		List<String> result = super.getDevelopmentTimesBundles();
		result.add("org.pragmaticmodeling.pxdoc.enablers.eclipse.emf");
		return result;
	}

	@Override
	public List<String> getRequiredBundles(boolean withPxdocLibs) {
		List<String> result = super.getRequiredBundles(withPxdocLibs);
		if (withPxdocLibs) {
			result.add("org.pragmaticmodeling.pxdoc.common.lib");
			result.add("org.pragmaticmodeling.pxdoc.emf.lib");
		}
		result.add("org.eclipse.emf.ecore");
		return result;
	}

}
