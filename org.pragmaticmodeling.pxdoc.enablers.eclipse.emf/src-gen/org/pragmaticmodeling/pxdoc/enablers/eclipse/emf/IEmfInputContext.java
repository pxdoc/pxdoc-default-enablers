package org.pragmaticmodeling.pxdoc.enablers.eclipse.emf;

import org.pragmaticmodeling.pxdoc.enablers.eclipse.IEclipseUiContext;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;

@SuppressWarnings("all")
public interface IEmfInputContext extends IEclipseUiContext {
  @PxGenParameter
  public abstract String getEClass();
  
  @PxGenParameter
  public abstract void setEClass(final String eClass);
  
  @PxGenParameter
  public abstract void setCommandId(final String commandId);
  
  @PxGenParameter
  public abstract String getCommandClassName();
  
  @PxGenParameter
  public abstract void setCommandClassName(final String commandClassName);
  
  public abstract String generatePluginXmlEntry();
  
  public abstract String getCommandId();
}
