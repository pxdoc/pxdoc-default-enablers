package org.pragmaticmodeling.pxdoc.runtime.eclipse.xtext;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.xtext.resource.EObjectAtOffsetHelper;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.ui.editor.XtextEditor;
import org.eclipse.xtext.ui.editor.outline.IOutlineNode;
import org.eclipse.xtext.ui.editor.utils.EditorUtils;
import org.eclipse.xtext.util.concurrent.IUnitOfWork;
import org.pragmaticmodeling.pxdoc.runtime.ui.eclipse.commands.AbstractPxDocWizardCommand;

import com.google.inject.Inject;

public abstract class AbstractPxDocXtextWizardCommand extends AbstractPxDocWizardCommand {

	private static Logger logger = Logger.getLogger(AbstractPxDocXtextWizardCommand.class);
	@Inject
	protected EObjectAtOffsetHelper eObjectAtOffsetHelper;

	@Override
	protected Object resolveInputModel(ExecutionEvent event) {
		List<Object> resolvedModel = new ArrayList<Object>();
		ISelection selection = getCurrentStructuredSelection(event);
		this.selectedModel = null;
		// FIXME pourquoi limiter la sélection à un seul élément ?
		if (selection != null && selection instanceof IStructuredSelection && !((IStructuredSelection) selection).isEmpty()) {
			Object currentSelection = ((IStructuredSelection) selection).getFirstElement();
			// FIXME EObjectNodes are not managed yet
			if (!(currentSelection instanceof IOutlineNode)) {
				resolvedModel.add(currentSelection);
			}
		}
		// }
		if (resolvedModel.isEmpty()) {
			try {
				XtextEditor editor = EditorUtils.getActiveXtextEditor(event);
				if (editor != null) {
					final ITextSelection textSelection = (ITextSelection) editor.getSelectionProvider().getSelection();
					editor.getDocument().priorityReadOnly(new IUnitOfWork.Void<XtextResource>() {
						@Override
						public void process(XtextResource state) throws Exception {
							EObject target = eObjectAtOffsetHelper.resolveElementAt(state, textSelection.getOffset());
							if (target != null)
								resolvedModel.add(target);
							// findReferences(target);
						}
					});
				}
			} catch (Exception e) {
				// FIXME si la commande n'est pas lancée depuis un module guice,
				// le code ci-dessus plante de toute façon...
				logger.error(e);
			}
		}
		return resolvedModel.isEmpty() ? null : resolvedModel.get(0);
	}

}
