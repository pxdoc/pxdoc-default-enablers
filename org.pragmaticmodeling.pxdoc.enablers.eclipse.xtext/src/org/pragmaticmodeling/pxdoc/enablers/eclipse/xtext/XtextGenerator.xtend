package org.pragmaticmodeling.pxdoc.enablers.eclipse.xtext

class XtextGenerator {
	
	def String contributePluginXml(String commandId, String label, String handler, String className, String qualifiedPxDocExecutableExtensionFactory, String xtextExtension) {
		'''	<!-- Commands -->
	<extension point="org.eclipse.ui.commands">
		<command
			id="«commandId»"
			name="«label»">
		</command>
	</extension>
	
	<!-- Handlers -->
	<extension point="org.eclipse.ui.handlers">
		<handler
			class="«qualifiedPxDocExecutableExtensionFactory»:«handler»"
			commandId="«commandId»">
		</handler>
	</extension>
	
	<!-- Menus -->
	<extension point="org.eclipse.ui.menus">
		<menuContribution
			allPopups="true"
			locationURI="popup:fr.pragmaticmodeling.common.menu">
			<command
				commandId="«commandId»"
				label="«label»"
				style="push">
						<visibleWhen
							checkEnabled="false">
							<or>
								<adapt type="org.eclipse.core.resources.IResource">
									<or>
										<test property="org.eclipse.core.resources.name" value="*.«xtextExtension»"/> 
									</or>
								</adapt>
								<with variable="activeEditorInput">
									<adapt type="org.eclipse.core.resources.IResource">
										<or>
											<test property="org.eclipse.core.resources.name" value="*.«xtextExtension»"/> 
										</or>
									</adapt>
								</with>
							</or>
						</visibleWhen>
			</command>
		</menuContribution>
	</extension>
		'''
	}
	
	def String commandClass(String packageName, String commandClass, String wizardClassName, String generatorClassName, String prettyGeneratorName, String inputClassFqn, String inputClassName) {
		'''
	package «packageName»;
	import org.eclipse.emf.ecore.EObject;
	import fr.pragmaticmodeling.pxdoc.util.EcoreUtil2;
	import «inputClassFqn»;
	import fr.pragmaticmodeling.pxdoc.runtime.AbstractPxDocGenerator;
	import org.pragmaticmodeling.pxdoc.enablers.eclipse.xtext.AbstractPxDocXtextCommand;
	import fr.pragmaticmodeling.pxdoc.runtime.IModelProvider;
	import fr.pragmaticmodeling.pxdoc.runtime.eclipse.ui.IPxDocWizard;
	import fr.pragmaticmodeling.pxdoc.runtime.launcher.PxDocLauncher;
	
	public class «commandClass» extends AbstractPxDocXtextCommand {
	
		@Override
		public IPxDocWizard createWizard(Object selection) {
			return new «wizardClassName»(createGenerator(), selection, getModelProvider());
		}
		
		@Override
		protected void initLauncher(PxDocLauncher launcher) {
			super.initLauncher(launcher);
			launcher.setPluginId(Activator.PLUGIN_ID);
		}
		
		@Override
		public AbstractPxDocGenerator createGenerator() {
			AbstractPxDocGenerator pxDocGenerator = new «generatorClassName»("«prettyGeneratorName»");
			initLauncher(pxDocGenerator.getLauncher());
			return pxDocGenerator;
		}

		@Override
		public IModelProvider getModelProvider() {
			return new IModelProvider() {
		
				@Override
				public Object adaptSelection(Object arg0) {
					if (arg0 instanceof EObject) {
						EObject model = EcoreUtil2.getContainerOfType((EObject)arg0, «inputClassName».class);
						if (model != null) {
							return model;
						}
					}
					return arg0;
				}
			};
		}
	}
		'''
	}
}
