package org.pragmaticmodeling.pxdoc.enablers.eclipse.xtext;

import fr.pragmaticmodeling.pxdoc.dsl.ui.enablers.AbstractPlatformEnabler;

public class XtextEnabler extends AbstractPlatformEnabler {

	public XtextEnabler() {
		super();
	}
	
	@Override
	public String getModelObject() {
		return "org.eclipse.emf.ecore.EObject";
	}

	@Override
	public String getJavaClass() {
		return "org.pragmaticmodeling.pxdoc.enablers.eclipse.xtext.XtextInput";
	}

	
}
