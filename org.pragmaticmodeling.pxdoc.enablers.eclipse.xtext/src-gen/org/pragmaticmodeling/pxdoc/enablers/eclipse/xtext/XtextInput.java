package org.pragmaticmodeling.pxdoc.enablers.eclipse.xtext;

import com.google.inject.Injector;
import fr.pragmaticmodeling.pxdoc.dsl.pxDoc.PxDocGenerator;
import fr.pragmaticmodeling.pxdoc.generator.RuntimeProject;
import java.io.File;
import java.util.List;
import org.apache.log4j.Logger;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.util.TypeReferences;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.EclipseUiPrj;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.emf.IEmfInputContext;
import org.pragmaticmodeling.pxdoc.enablers.eclipse.xtext.IXtextInputContext;
import org.pragmaticmodeling.pxgen.runtime.AbstractGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.IGeneratorFragment;
import org.pragmaticmodeling.pxgen.runtime.PxGenParameter;
import org.pragmaticmodeling.pxgen.runtime.projects.ProjectDescriptor;

@SuppressWarnings("all")
public class XtextInput extends AbstractGeneratorFragment implements IXtextInputContext {
  public XtextInput() {
    super();
  }
  
  public IEmfInputContext getParentContext() {
    return (IEmfInputContext)getParentFragment();
  }
  
  public String generatePluginEntry() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("\t");
    _builder.append("<!-- Commands -->");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<extension point=\"org.eclipse.ui.commands\">  ");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<command");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("id=\"");
    String _commandId = this.getCommandId();
    _builder.append(_commandId, "\t\t\t");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("name=\"");
    _builder.append("\">");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("</command>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</extension>");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<!-- Handlers -->");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<extension point=\"org.eclipse.ui.handlers\">");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<handler");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("class=\"");
    String _executableExtensionFactoryClassName = this.getExecutableExtensionFactoryClassName();
    _builder.append(_executableExtensionFactoryClassName, "\t\t\t");
    _builder.append(":");
    String _commandClassName = this.getCommandClassName();
    _builder.append(_commandClassName, "\t\t\t");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t");
    _builder.append("commandId=\"");
    String _commandId_1 = this.getCommandId();
    _builder.append(_commandId_1, "\t\t\t");
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t");
    _builder.append("</handler>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</extension>");
    _builder.newLine();
    _builder.append("\t");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<!-- Menus -->");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("<extension point=\"org.eclipse.ui.menus\">");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("<menuContribution");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("allPopups=\"true\"");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("locationURI=\"popup:fr.pragmaticmodeling.common.menu\">");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("<command");
    _builder.newLine();
    _builder.append("\t\t\t\t");
    _builder.append("commandId=\"");
    String _commandId_2 = this.getCommandId();
    _builder.append(_commandId_2, "\t\t\t\t");
    _builder.append("\"");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t");
    _builder.append("label=\"");
    String _menuLabel = this.getMenuLabel();
    _builder.append(_menuLabel, "\t\t\t\t");
    _builder.append("\" ");
    _builder.newLineIfNotEmpty();
    _builder.append("\t\t\t\t");
    _builder.append("style=\"push\">");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t");
    _builder.append("<visibleWhen   ");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t\t");
    _builder.append("checkEnabled=\"false\">");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t\t");
    _builder.append("<iterate>");
    _builder.newLine();
    _builder.append("                        ");
    _builder.append("<instanceof");
    _builder.newLine();
    _builder.append("                              ");
    _builder.append("value=\"");
    String _eClass = this.getEClass();
    _builder.append(_eClass, "                              ");
    _builder.append("\">");
    _builder.newLineIfNotEmpty();
    _builder.append("                        ");
    _builder.append("</instanceof>");
    _builder.newLine();
    _builder.append("                     ");
    _builder.append("</iterate>");
    _builder.newLine();
    _builder.append("\t\t\t\t\t\t");
    _builder.append("</visibleWhen>");
    _builder.newLine();
    _builder.append("\t\t\t");
    _builder.append("</command>");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("</menuContribution>");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("</extension>");
    return _builder.toString();
  }
  
  @PxGenParameter
  public void setEClass(final String eClass) {
    getParentContext().setEClass(eClass);
  }
  
  @PxGenParameter
  public String getEClass() {
    return getParentContext().getEClass();
  }
  
  @PxGenParameter
  public void setCommandId(final String commandId) {
    getParentContext().setCommandId(commandId);
  }
  
  @PxGenParameter
  public void setCommandClassName(final String commandClassName) {
    getParentContext().setCommandClassName(commandClassName);
  }
  
  @PxGenParameter
  public String getCommandClassName() {
    return getParentContext().getCommandClassName();
  }
  
  public String generatePluginXmlEntry() {
    return getParentContext().generatePluginXmlEntry();
  }
  
  public String getCommandId() {
    return getParentContext().getCommandId();
  }
  
  @PxGenParameter
  public void setPluginEntries(final List<String> pluginEntries) {
    getParentContext().setPluginEntries(pluginEntries);
  }
  
  @PxGenParameter
  public List<String> getPluginEntries() {
    return getParentContext().getPluginEntries();
  }
  
  @PxGenParameter
  public void setExecutableExtensionFactoryClassName(final String executableExtensionFactoryClassName) {
    getParentContext().setExecutableExtensionFactoryClassName(executableExtensionFactoryClassName);
  }
  
  @PxGenParameter
  public String getExecutableExtensionFactoryClassName() {
    return getParentContext().getExecutableExtensionFactoryClassName();
  }
  
  @PxGenParameter
  public void setSelectionMode(final String selectionMode) {
    getParentContext().setSelectionMode(selectionMode);
  }
  
  @PxGenParameter
  public String getSelectionMode() {
    return getParentContext().getSelectionMode();
  }
  
  @PxGenParameter
  public void setBaseCommandClass(final String baseCommandClass) {
    getParentContext().setBaseCommandClass(baseCommandClass);
  }
  
  @PxGenParameter
  public String getBaseCommandClass() {
    return getParentContext().getBaseCommandClass();
  }
  
  @PxGenParameter
  public void setCommandClassFqn(final String commandClassFqn) {
    getParentContext().setCommandClassFqn(commandClassFqn);
  }
  
  @PxGenParameter
  public String getCommandClassFqn() {
    return getParentContext().getCommandClassFqn();
  }
  
  @PxGenParameter
  public void setAbstractCommandConstructors(final String abstractCommandConstructors) {
    getParentContext().setAbstractCommandConstructors(abstractCommandConstructors);
  }
  
  @PxGenParameter
  public String getAbstractCommandConstructors() {
    return getParentContext().getAbstractCommandConstructors();
  }
  
  @PxGenParameter
  public void setAbstractCommandMethods(final String abstractCommandMethods) {
    getParentContext().setAbstractCommandMethods(abstractCommandMethods);
  }
  
  @PxGenParameter
  public String getAbstractCommandMethods() {
    return getParentContext().getAbstractCommandMethods();
  }
  
  @PxGenParameter
  public void setCommandConstructors(final String commandConstructors) {
    getParentContext().setCommandConstructors(commandConstructors);
  }
  
  @PxGenParameter
  public String getCommandConstructors() {
    return getParentContext().getCommandConstructors();
  }
  
  @PxGenParameter
  public void setMenuLabel(final String menuLabel) {
    getParentContext().setMenuLabel(menuLabel);
  }
  
  @PxGenParameter
  public String getMenuLabel() {
    return getParentContext().getMenuLabel();
  }
  
  @PxGenParameter
  public void setPropertyTesterFqn(final String propertyTesterFqn) {
    getParentContext().setPropertyTesterFqn(propertyTesterFqn);
  }
  
  @PxGenParameter
  public String getPropertyTesterFqn() {
    return getParentContext().getPropertyTesterFqn();
  }
  
  @PxGenParameter
  public void setBaseModelProvider(final String baseModelProvider) {
    getParentContext().setBaseModelProvider(baseModelProvider);
  }
  
  @PxGenParameter
  public String getBaseModelProvider() {
    return getParentContext().getBaseModelProvider();
  }
  
  @PxGenParameter
  public void setAbstractCommandClassBody(final String abstractCommandClassBody) {
    getParentContext().setAbstractCommandClassBody(abstractCommandClassBody);
  }
  
  public String getCommandObject() {
    return getParentContext().getCommandObject();
  }
  
  public JvmTypeReference findJvmRef(final String fqn) {
    return getParentContext().findJvmRef(fqn);
  }
  
  public String getAbstractCommandClassBody() {
    return getParentContext().getAbstractCommandClassBody();
  }
  
  public EclipseUiPrj getEclipseUiPrj() {
    return getParentContext().getEclipseUiPrj();
  }
  
  public String simpleName(final String qualifiedName) {
    return getParentContext().simpleName(qualifiedName);
  }
  
  public String javaPackage(final String qualifiedName) {
    return getParentContext().javaPackage(qualifiedName);
  }
  
  public String generateExtensions() {
    return getParentContext().generateExtensions();
  }
  
  @PxGenParameter
  public void setBasePackage(final String basePackage) {
    getParentContext().setBasePackage(basePackage);
  }
  
  @PxGenParameter
  public String getBasePackage() {
    return getParentContext().getBasePackage();
  }
  
  @PxGenParameter
  public void setPxDocUri(final String pxDocUri) {
    getParentContext().setPxDocUri(pxDocUri);
  }
  
  @PxGenParameter
  public String getPxDocUri() {
    return getParentContext().getPxDocUri();
  }
  
  @PxGenParameter
  public void setPxDocGenerator(final PxDocGenerator pxDocGenerator) {
    getParentContext().setPxDocGenerator(pxDocGenerator);
  }
  
  @PxGenParameter
  public void setStylesheet(final File stylesheet) {
    getParentContext().setStylesheet(stylesheet);
  }
  
  @PxGenParameter
  public File getStylesheet() {
    return getParentContext().getStylesheet();
  }
  
  @PxGenParameter
  public void setGeneratorClassName(final String generatorClassName) {
    getParentContext().setGeneratorClassName(generatorClassName);
  }
  
  @PxGenParameter
  public String getGeneratorClassName() {
    return getParentContext().getGeneratorClassName();
  }
  
  @PxGenParameter
  public void setLogger(final Logger logger) {
    getParentContext().setLogger(logger);
  }
  
  @PxGenParameter
  public Logger getLogger() {
    return getParentContext().getLogger();
  }
  
  @PxGenParameter
  public void setResourceSet(final XtextResourceSet resourceSet) {
    getParentContext().setResourceSet(resourceSet);
  }
  
  @PxGenParameter
  public XtextResourceSet getResourceSet() {
    return getParentContext().getResourceSet();
  }
  
  @PxGenParameter
  public void setTypeReferences(final TypeReferences typeReferences) {
    getParentContext().setTypeReferences(typeReferences);
  }
  
  @PxGenParameter
  public TypeReferences getTypeReferences() {
    return getParentContext().getTypeReferences();
  }
  
  public Object addDevelopmentTimeBundles(final IGeneratorFragment f) {
    return getParentContext().addDevelopmentTimeBundles(f);
  }
  
  public String getProjectName(final Class<?> clazz) {
    return getParentContext().getProjectName(clazz);
  }
  
  public RuntimeProject getRuntimeProject() {
    return getParentContext().getRuntimeProject();
  }
  
  public String getPxDocGeneratorClassName() {
    return getParentContext().getPxDocGeneratorClassName();
  }
  
  public PxDocGenerator getPxDocGenerator() {
    return getParentContext().getPxDocGenerator();
  }
  
  public PxDocGenerator loadGenerator() {
    return getParentContext().loadGenerator();
  }
  
  public Void setModelObject(final String object) {
    return getParentContext().setModelObject(object);
  }
  
  public void initialize(final Injector injector) {
    injector.injectMembers(this);
    for (ProjectDescriptor project : getProjects()) {
    	project.initialize(injector);
    }
    for (IGeneratorFragment f : getFragments()) {
    	f.initialize(injector);
    	//injector.injectMembers(f);
    }
  }
  
  @Override
  public void doGenerate() {
    {
      List<String> _pluginEntries = this.getPluginEntries();
      String _generatePluginXmlEntry = this.generatePluginXmlEntry();
      _pluginEntries.add(_generatePluginXmlEntry);
      List<String> _requiredBundles = this.getEclipseUiPrj().getRequiredBundles();
      _requiredBundles.add("org.pragmaticmodeling.pxdoc.runtime.enablers.eclipse.xtext");
    }
  }
}
