package org.pragmaticmodeling.pxdoc.enablers.eclipse.xtext;

import org.pragmaticmodeling.pxdoc.enablers.eclipse.emf.IEmfInputContext;

@SuppressWarnings("all")
public interface IXtextInputContext extends IEmfInputContext {
  public abstract String generatePluginEntry();
}
